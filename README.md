[![build status](https://gitlab.com/chrisbelyea/pipeline/badges/master/build.svg)](https://gitlab.com/chrisbelyea/pipeline/commits/master)
[![coverage report](https://gitlab.com/chrisbelyea/pipeline/badges/master/coverage.svg)](https://gitlab.com/chrisbelyea/pipeline/commits/master)

# GitLab CI Pipeline POC

This project is a proof-of-concept for using GitLab CI Pipelines to promote code.
